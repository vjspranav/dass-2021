# DASS-2021


## Introduction 
  This document serves as a dashboard of the 2021 - DASS projects.
 
## Name of the Company
  Virtual Labs Engineering and Architecture Division
## Primary Work Location
  Work From Home (online)
## Email
  engg@vlabs.ac.in ; 2021-ssad@vlabs.ac.in
## Projects Proposed
  - Project-1 - Porting of [Hydraulics and Fluid Mechanics](http://eerc03-iiith.vlabs.ac.in/) Virtual Labs to JavaScript 

  - Project-2 - Porting of [Structural Dynamics](http://sd-iiith.vlabs.ac.in/) Virtual Labs to JavaScript

  - Project-3 - Porting of [Colloid and Surface Chemistry](http://csc-iiith.vlabs.ac.in/) Virtual Labs to JavaScript

  - Project-4 - Porting of [Computer Organisation](http://cse11-iiith.vlabs.ac.in/) and [Physical Chemistry](http://ccnsb06-iiith.vlabs.ac.in/) Virtual Labs to JavaScript


  ## Project Tasks and Outcome
  
   All the above projects will involve the following tasks 

  ### Tasks   
   1. Study the lab (experiment) and understand the interface.
   
   2. Identify the porting changes.  

  ### Outcome 
  
  A document with recommendations on porting or even to consider pure JS based libraries.

  ### Tasks
   3. Complete Porting of all the experiments to JS. Follow the [link](https://github.com/virtual-labs/ph3-exp-dev-process/tree/main/best-practices) for best practices and colour themes to be followed during development.

   4. Test and fix bugs: 
     
  ### Outcome
  
  Lab (Experiment) should run in the browser along with **responsive design** feature and documentation of the process.

  ### Tasks
   5. Additional effort: Automate the testing if time permits.

  ## Sample Experiment 

  - **Lab Name :** Speech Signal Processing Lab 

  - **Exp Name :** Manual Speech Signal-to-Symbol Transformation

  - **Source Code :** [Link](https://github.com/virtual-labs/exp-manual-speech-signal-iiith)

  - **Hosted URL :** [Link](https://ssp-iiith.vlabs.ac.in/exp/speech-signal-to-symbol-transformation/) 


## Number of People
  Four students per project. 
## Technology
  Javascript, CSS and HTML
## Team Members and Contact Information  
  
  
  
    TA - Paryul Jain (paryul.jain@research.iiit.ac.in) 



| SNo  |Team ID   |Members in the team   | Email address/Phone Number  | Github Handle/Gitlab Handle  | Lab Name/Hosted Link/Non js repo link | Assigned Experiment names/repo links|
|---|---|---|---|---|---|---|
| 1.  | Project-1  | 1. VJS Pranavasri <br> 2. Jayant Panwar <br> 3. Arth Raj <br> 4. Aman   | 1. vjs.pranavasri@research.iiit.ac.in <br> 2. jayant.panwar@research.iiit.ac.in <br> 3. arth.raj@students.iiit.ac.in <br> 4. aman.rajmeel@students.iiit.ac.in   |   |Hydraulics and Fluid Mechanics / [Hosted-Link](http://eerc03-iiith.vlabs.ac.in/) / [Non-js-repo-link](https://github.com/virtual-labs/hydraulics-and-fluid-mechanics-iiith) <br><br> Physical Chemistry / [Hosted-Link](http://ccnsb06-iiith.vlabs.ac.in/) / [Non-js-repo-link](https://github.com/virtual-labs/physical-chemistry-iiith) |1. [Bernoulli's Experiment](https://github.com/virtual-labs/exp-bernoullis-iiith) <br>2. [Venturi Meter Experiment](https://github.com/virtual-labs/exp-venturi-meter-iiith) <br>3.[Orifices Experiment](https://github.com/virtual-labs/exp-orifices-iiith) <br>4. [Mouthpieces Experiment]( https://github.com/virtual-labs/exp-mouthpieces-iiith) <br>5. [Weirs Experiment](https://github.com/virtual-labs/exp-weirs-iiith) <br>6. [Channels Experiment](https://github.com/virtual-labs/exp-channels-iiith) <br> 7. [Reynold's Experiment](https://github.com/virtual-labs/exp-reynolds-iiith) <br>8. [Jets Experiment](https://github.com/virtual-labs/exp-jets-iiith) <br> 9. [Turbines Experiment](https://github.com/virtual-labs/exp-turbines-iiith)  <br> **Physical Chemistry** <br> 4. [Determination of molar mass of simple compounds using mass spectroscopy](https://github.com/virtual-labs/exp-determination-molar-mass-iiith)  | 
|  2. | Project-2  | 1. Dinesh Garg <br> 2. Yash Chauhan <br> 3. Debayan Saha <br> 4. Shaurya Dewan | 1. dinesh.garg@students.iiit.ac.in <br> 2. yash.chauhan@students.iiit.ac.in <br> 3. debayan.saha@research.iiit.ac.in <br> 4. shaurya.dewan@students.iiit.ac.in |   | Structural Dynamics / [Hosted-Link](http://sd-iiith.vlabs.ac.in/) / [Non-js-repo-link](https://github.com/virtual-labs/structural-dynamics-iiith)  | 1. [Simple Harmonic Oscillator](https://github.com/virtual-labs/exp-simple-harmonic-oscillator-iiith) <br>2. [Free Vibration of S.D.O.F System](https://github.com/virtual-labs/exp-free-vibration-iiith) <br>3. [Forced Vibration of S.D.O.F System](https://github.com/virtual-labs/exp-forced-vibration-iiith) <br>4. [Impulse Response of S.D.O.F System](https://github.com/virtual-labs/exp-impulse-response-iiith) <br>5. [Concept of Response Spectrum](https://github.com/virtual-labs/exp-response-spectrum-iiith) <br>6. [Vibration of M.D.O.F System](https://github.com/virtual-labs/exp-vibration-of-mdof-iiith) <br>7. [Behaviour of Rigid Blocks](https://github.com/virtual-labs/exp-behaviour-rigid-blocks-iiith) <br>8. [Torsional Response of Building](https://github.com/virtual-labs/exp-torsional-response-iiith) <br>9. [Continuous Systems](https://github.com/virtual-labs/exp-continuous-systems-iiith) <br>10. [Vibration Control](https://github.com/virtual-labs/exp-vibration-control-iiith)  | 
|  3 |  Project-3 | 1. Harshdeep Singh <br>  2. Kavya Kolli <br> 3. Umang Srivastava <br> 4. Nitin Chandak | 1. harshdeep.singh@research.iiit.ac.in <br> 2. kavya.kolli@students.iiit.ac.in <br> 3. umang.srivastava@students.iiit.ac.in <br> 4. nitin.chandak@students.iiit.ac.in   |   | Colloid and Surface Chemistry Virtual Lab / [Hosted-Link](http://csc-iiith.vlabs.ac.in/) / [Non-js-repo-link](https://github.com/virtual-labs/colloid-and-surface-chemistry-iiith) <br><br> Physical Chemistry / [Hosted-Link](http://ccnsb06-iiith.vlabs.ac.in/) / [Non-js-repo-link](https://github.com/virtual-labs/physical-chemistry-iiith) |1. [Preparation of Sols](https://github.com/virtual-labs/exp-preparation-of-sols-iiith) <br> 2. [Preparation of Gels](https://github.com/virtual-labs/exp-preparation-of-gels-iiith) <br> 3. [Demonstration of the Preparation and Use of Association Colloids (Micelles)](https://github.com/virtual-labs/exp-association-colloids-iiith) <br> 4. [Determination of Critical Micelle Concentration (CMC) of a Surfactant](https://github.com/virtual-labs/exp-critical-micelle-concentration-iiith) <br> 5. [Demonstration of the Surface Tension Lowering of Water by Soaps/Detergents](https://github.com/virtual-labs/exp-surface-tension-lowering-iiith) <br> 6. [Demonstration of Tyndall Effect or Tyndall Scattering in Colloidal Systems](https://github.com/virtual-labs/exp-tyndall-scattering-iiith) <br> 7. [Gel Electrophoresis](https://github.com/virtual-labs/exp-gel-electrophoresis-iiith) <br>8. [Demonstration of the Surface-Effect on Chemical Properties of Finely Divided Particles](https://github.com/virtual-labs/exp-surface-effect-iiith) <br>9. [Study of the Catalytic Effects of Finely Divided Particles](https://github.com/virtual-labs/exp-catalytic-effects-iiith) <br> **Physical Chemistry** <br> 5. [Nuclear magnetic resosnace spectrocopy and evaulation of simple 1H NMR spectra of select organic compounds](https://github.com/virtual-labs/exp-nuclear-magnetic-resosnace-spectrocopy-iiith)   | 
|  4 | Project-4  | 1. Pranjal Jain <br> 2. Stella Sravanthi <br> 3. Aryaaps Dubey <br> 4. Shreyansh Verma  | 1. pranjal.jain@students.iiit.ac.in <br> 2. stella.sravanthi@students.iiit.ac.in <br> 3. aryan.dubey@research.iiit.ac.in <br> 4. shreyansh.verma@research.iiit.ac.in  |   | Computer Organisation / [Hosted-Link](http://cse11-iiith.vlabs.ac.in/) / [Non-js-repo-link](https://github.com/virtual-labs/computer-organization-iiith) <br><br> Physical Chemistry / [Hosted-Link](http://ccnsb06-iiith.vlabs.ac.in/) / [Non-js-repo-link](https://github.com/virtual-labs/physical-chemistry-iiith)  | <br> **Computer Organisation:** <br> 1. [Representation of Integers and their Arithmetic](https://github.com/virtual-labs/exp-Integers-arithmetic-iiith)  <br> 2. [Representation of Floating Point Numbers and their Arithmetic](https://github.com/virtual-labs/exp-floating-point-numbers-iiith) <br> 3. [Virtual Memory](https://github.com/virtual-labs/exp-virtual-memory-iiith) <br> 4. [MIPS Assembly Language Programming - 1](https://github.com/virtual-labs/exp-mips1-iiith) <br> 5. [ARM Assembly Language Programming - 1](https://github.com/virtual-labs/exp-arm1-iiith) <br> 6. [ARM Assembly Language Programming - 2](https://github.com/virtual-labs/exp-arm2-iiith) <br> 7. [Single Cycle MIPS CPU](https://github.com/virtual-labs/exp-single-cycle-mips-iiith) <br> **Physical Chemistry:** <br> 1. [Instrumentation and working principles of infra red (IR) spectroscopy using salt plates](https://github.com/virtual-labs/exp-infra-red-spectroscopy-iiith) <br> 2. [Instrumentation and working principles of solutions infra red (IR) spectroscopy](https://github.com/virtual-labs/exp-solutions-infra-red-spectroscopy-iiith) <br> 3. [Instrumentation and working principles principles of mass spectroscopy](https://github.com/virtual-labs/exp-mass-spectroscopy-iiith)    <br> 6. [Identification of unknown components using spectroscopic techniques](https://github.com/virtual-labs/exp-spectroscopic-techniques-iiith)  | 

  
## Working Guidelines 

### Development Process 

Step 1 : Choose one exp from the assigned list and send a mail to systems@vlabs.ac.in with your Github handle and associated email id requesting to be added to the repository.  

Step 2 : Clone the repository.

Step 3 : Populate the dev branches of the repository with the source code of the experiments and unit test the experiments locally. Do not delete gh-pages branch. This is required for automatically deploying the experiment along with UI on GitHub pages for testing the complete experiment .Also do not delete .github directory and the LICENSE file.

Step 4 : Merge the fully tested dev branch to testing branch. This will automatically deploy the experiment along with UI on GitHub pages for testing the complete experiment. You will receive an email about the success/failure of the deployment on the email id associated with the github handle provided in Step 1. 

Step 5 : Set up a demo with the Virtual Labs Team and merge the code with **main branch** only after getting approval from them. 


### Realization in terms of Milestones
  Follow [agile](https://en.wikipedia.org/wiki/Agile_software_development) software development and use [scrum](https://www.scrumalliance.org/why-scrum)  methodology to incrementally working software.  Every project has a product backlog.  In each sprint, items from the product backlog are picked to be realized in a milestone.  Each item from a product backlog translates to one or more tasks and each task is tracked as an issue on
  github.  A milestone is a collection of issues.  It is upto the mentor and the team to choose either one or two week sprints.

  Each release is working software and a release determines the achievement of a milestone.  A project is realized as a series of milestones.

  This planning will be part of the master repo of respective project repos.
  
### Communication 
  Every discussion about the project will be through issues.  Mails are not used for discussion.
  Any informal communication will be through Slack - DASS=2021 channel.

### Total person hours  ( As committed by the course coordinator) 
   Design and Analysis of Software Systems (DASS, formerly SSAD) course for the 2nd year students to work on. As you know, the project component carries 40% weightage for the course, and it would be great if the students would get an opportunity to work with you and the team as their
   clients.

   Each student expected to devote 10 hours per week for their projects. It is not 10 hours per team, rather 10 hours per student. Just confirmed it with Prof. Ramesh. There are a total of 12 productive weeks that the student must work for the project. (3 in January, 3 in February, 4 in March and 2 in April). This is excluding the mid-semester week, as that wouldn't be a productive one. There would be 4 members per team, so each team devotes
   40 hours per week and 480 hours in total for the project.

   Since these project are meant to simulate a proper industrial project, they would have to sincerely meet with the clients regularly, document everything, keep track of their status on git and most importantly complete the project and get a good client feedback, else they'd get a bad grade! Kindly requesting you to open up some Virtual Labs projects for the students to work on.
   
### Things done at the weekly meeting

  1. Every meeting starts with a presentation of the code documents and a demonstration.

  2. The pull/merge request is reviewed at the meeting.

  3. Task planning for the next week is discussed.  The students take the meeting notes.  The meeting notes has
     the below structure.  From the meeting, action items are thrashed out and are listed.  The mom file is also part of this repository.  Sample mom is placed under [meetings](https://gitlab.com/vlead-projects/dass-2020/tree/master/%20meeting) directory. You may also refer to the [mom](https://gitlab.com/vlead-projects/experiments/2018-ssad/index/blob/master/src/meetings/2018-08-31-mom.org)
     which captures the mom of all the teams. 

   
     ** Agenda

     ** Discussion Points

     ** Action

     |   |   |   |   |   
     |---|---|---|---| 
     | Item  |  Issue | Artifact   |Status    |   
     | Implement filter|Link to| link to file|In progress|issue |in the repo 


### Work logs
  The work logs of each student will need to be pushed to the repository shared by the TA/Course Coordinator. The work-logs will consider for Grading. 

### Team Meetings
  Team meetings will be held every Wednesday from 7:00 PM till 8:00 PM with a slotted time of 15 min per team.
  


